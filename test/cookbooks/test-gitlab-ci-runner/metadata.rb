name 'test-gitlab-ci-runner'
maintainer 'Sam4Mobile'
maintainer_email 'dps.team@s4m.io'
license 'Apache 2.0'
description 'Helper Cookbook to test Gitlab CI Runner'
long_description 'Helper Cookbook to test Gitlab CI Runner'
source_url 'https://gitlab.com/s4m-chef-repositories/gitlab-ci-runner'
issues_url 'https://gitlab.com/s4m-chef-repositories/gitlab-ci-runner/issues'
version '1.0.0'

supports 'centos', '>= 7.1'

depends 'gitlab-ci-runner'
